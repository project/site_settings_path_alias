<?php

namespace Drupal\site_settings_path_alias;

use Drupal\Core\Form\FormStateInterface;

/**
 * SiteInformationSettingsManager service.
 */
class SiteInformationSettingsManager {

  protected const DEFAULT_VALIDATE_FUNCTION = "::validateForm";

  /**
   * Alters form and sets custom validation form.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form status.
   */
  public function alterForm(array &$form, FormStateInterface $form_state) {
    foreach ($form['#validate'] as &$validation) {
      if ($validation == self::DEFAULT_VALIDATE_FUNCTION) {
        $validation = '\Drupal\site_settings_path_alias\SiteInformationSettingsManager::customValidateForm';
      }
    }
  }

  /**
   * Validates the site configuration form without converting path aliases.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form status.
   */
  public static function customValidateForm(array &$form, FormStateInterface $form_state) {
    // Check for empty front page path.
    if ($form_state->isValueEmpty('site_frontpage')) {
      // Set to default "user/login".
      $form_state->setValueForElement($form['front_page']['site_frontpage'], '/user/login');
    }
    // Validate front page path.
    if (($value = $form_state->getValue('site_frontpage')) && $value[0] !== '/') {
      $form_state->setErrorByName('site_frontpage', t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_frontpage')]));

    }
    if (!\Drupal::service('path.validator')->isValid($form_state->getValue('site_frontpage'))) {
      $form_state->setErrorByName('site_frontpage', t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('site_frontpage')]));
    }

    self::customErrorPagesValidation($form, $form_state);
  }

  /**
   * Validates the error pages configuration without converting path aliases.
   *
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form status.
   */
  protected static function customErrorPagesValidation(array &$form, FormStateInterface $form_state) {
    if (($value = $form_state->getValue('site_403')) && $value[0] !== '/') {
      $form_state->setErrorByName('site_403', t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_403')]));
    }
    if (($value = $form_state->getValue('site_404')) && $value[0] !== '/') {
      $form_state->setErrorByName('site_404', t("The path '%path' has to start with a slash.", ['%path' => $form_state->getValue('site_404')]));
    }
    // Validate 403 error path.
    if (!$form_state->isValueEmpty('site_403') && !\Drupal::service('path.validator')->isValid($form_state->getValue('site_403'))) {
      $form_state->setErrorByName('site_403', t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('site_403')]));
    }
    // Validate 404 error path.
    if (!$form_state->isValueEmpty('site_404') && !\Drupal::service('path.validator')->isValid($form_state->getValue('site_404'))) {
      $form_state->setErrorByName('site_404', t("Either the path '%path' is invalid or you do not have access to it.", ['%path' => $form_state->getValue('site_404')]));
    }
  }

}
